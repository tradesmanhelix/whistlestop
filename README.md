# README

## RUNNING

In order to run this app, you'll need to have Rails installed. Once you do, navigate to the `bin/` folder in this project and enter this command to get this application up and running:

    $ bin/rails server

Then, visit [http://localhost:3000](http://localhost:3000) in your web browser.


## PROJECT INFO

* Ruby version: `ruby 2.1.2p95 (2014-05-08 revision 45877)`

* System dependencies: None (runs on vanilla Rails install)

* Configuration: App listens on port `3000` by default. You may need to tweak this if another app uses that port on your machine.

* Database: `SQLite3`

* Database initialization: 
    * For dev: `$ bin/rake db:migrate`
    * For prod: `$ bin/rake db:migrate RAILS_ENV=production`


## CREDITS

I initially created this app by following the Getting Started with Rails guide located @ [http://guides.rubyonrails.org/getting_started.html](http://guides.rubyonrails.org/getting_started.html). Any and all customizations are my own.
